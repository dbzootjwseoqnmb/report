<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DisplayController extends Controller
{
    private $coreController = null;
    private $vStatus = array();
    private $tempVStatus =  array(
        'Critical' => [
            'open' => 0,
            're-opened' => 0,
            'closed' => 0,
            'risk-accepted' => 0
        ],
        'High' => [
            'open' => 0,
            're-opened' => 0,
            'closed' => 0,
            'risk-accepted' => 0
        ],
        'Medium' => [
            'open' => 0,
            're-opened' => 0,
            'closed' => 0,
            'risk-accepted' => 0
        ],
        'Low' => [
            'open' => 0,
            're-opened' => 0,
            'closed' => 0,
            'risk-accepted' => 0
        ],
        'Informational' => [
            'open' => 0,
            're-opened' => 0,
            'closed' => 0,
            'risk-accepted' => 0
        ],
        'Test case' => [
            'open' => 0,
            're-opened' => 0,
            'closed' => 0,
            'risk-accepted' => 0
        ],
    );
    private $workspace = "";
//    private $workspace = "sample-client-workspace";
    private $workspaces = null;


    public function __construct(){
        $this->coreController = new CoreController();
    }

    public function dashboard(){
        $this->workspaces = json_decode($this->coreController->getAllWorkSpaces());
        $workspaces = $this->workspaces;
        return view('dashboard', compact('workspaces'));
    }
    private function getTitle($titles, $workspaces, $searchws){
        foreach($workspaces as $index => $workspace) {
            if($searchws == $workspace) {
                return $titles[$index];
            }
        }
    }
    public function genreport(Request $request){

        $colors = array(
            'critical_bg' => $request->critical_bg,
            'critical_txt' => $request->critical_txt,
            'high_bg' => $request->high_bg,
            'high_txt' => $request->high_txt,
            'medium_bg' => $request->medium_bg,
            'medium_txt' => $request->medium_txt,
            'low_bg' => $request->low_bg,
            'low_txt' => $request->low_txt,
            'info_bg' => $request->info_bg,
            'info_txt' => $request->info_txt,
            'test_bg' =>  $request->test_bg,
            'test_txt' => $request->test_txt
        );
        $status_colors = array(
            'open_bg' => $request->open_bg,
            'open_txt' => $request->open_txt,
            'reopen_bg' => $request->reopen_bg,
            'reopen_txt' => $request->reopen_txt,
            'closed_bg' => $request->closed_bg,
            'closed_txt' => $request->closed_txt,
            'riskack_bg' => $request->riskack_bg,
            'riskack_txt' => $request->riskack_txt,

        );
        $text_font = "Verdana, Geneva, sans-serif";
        $ws = array();
        foreach($request->checkboxes as $searchws){
            $ws[] = array(
                'title' => $this->getTitle($request->titles, $request->workspaces, $searchws),
                'workspace' => $searchws
            );
        }
        $font_family = $request->font_family;

        if(isset($request->excel)) {
            return $this->generateWord($ws, $colors, $font_family, $status_colors, true);
        }
        if(isset($request->word)){
            return $this->generateWord($ws, $colors, $font_family, $status_colors);
        }
    }

    private function generateWord($workspaces, $colors, $font_family, $status_colors, $excel = false){
//        dd($workspaces);
        $finalStatus = array();
        foreach($workspaces as $workspace) {
            $vulnerabilities = $this->coreController->getAllVulnerabilities($workspace['workspace']);
            $hosts = $this->coreController->getAllHosts($workspace['workspace']);
            $title = $workspace['title'];
            $this->workspace = $workspace['workspace'];
            $vulns = $this->prepare($hosts, $vulnerabilities, $title, $colors, $status_colors);
            $final[] = array(
                'title' => $title,
                'report' => $vulns
            );
//            $vulnerabilityStatus = $this->getVulnerabilityStatus($vulnerabilities);
//            $finalStatus  = array_merge($finalStatus, $vulnerabilityStatus);
//            array_push($finalStatus, $vulnerabilityStatus);
        }
//        dd($final);
//        dd($finalStatus);
        $vStatus = $this->vStatus;
        if(!$excel)  return view('report', compact('final', 'colors', 'font_family', 'status_colors', 'vStatus'));
        else         return view('excel', compact('final', 'colors', 'font_family', 'status_colors', 'vStatus'));


    }

    private function setVulnerabilityStatus($host, $severity, $status): void{
//        dd($host, $severity, $status);
        if(!isset($this->vStatus[$host])) {
            //this is first time this host is being added
            $this->vStatus[$host] = $this->tempVStatus;
            $this->vStatus[$host][$severity][$status] += 1;

        } else  {
            //main array has already been updated
            $this->vStatus[$host][$severity][$status] += 1;
        }

    }
    public function doLogin(Request $request){
        $token = $request->token;
        if($this->coreController->checkLogin($token)){
            Session::put('token', $token);
            return redirect()->route('dashboard');
        } else {
            $error = 'invalid token';
            return view('login', compact('error'));
        }
    }
    public function display(){
//        $workspace = "pmd_re-assessment_2021";
//        $hosts = $this->coreController->getAllHosts($this->workspace);
        $vulnerabilities = $this->coreController->getAllVulnerabilities($this->workspace);
        $hosts = $this->coreController->getAllHosts($this->workspace);
        $vulns = $this->prepare($hosts, $vulnerabilities);
        return view('report', compact('vulns'));
    }

    private function checkAttachments($vuln){
        $r = $this->coreController->getAttachment($this->workspace, $vuln->id);
        return json_decode($r) ;
    }
    private function setSeverity($severity){
        if($severity == "info")
            return "informational";
        if($severity == "med")
            return "medium";
        if($severity == "unclassified")
            return "test case";
        return $severity;
    }
    private function prepare($hosts, $vulns, $title, $colors, $status_colors){
        $finalResult = array();
        $report = json_decode($vulns);
        $hosts_details = json_decode($hosts);
//        dd($hosts_details);
        foreach ($report->vulnerabilities as $vulnerability) {
            $host = $vulnerability->value->target;
            $severity = ucfirst($this->setSeverity($vulnerability->value->severity));
            $details = array();
            $details['description'] = $vulnerability->value->description;
            $details['cve'] = $vulnerability->value->cve;
            $details['name'] = $vulnerability->value->name;
            $details['status'] = ucwords($vulnerability->value->status);
            $details['resolution'] = $vulnerability->value->resolution;
            $details['attachments'] = $this->checkAttachments($vulnerability);
            $details['impacted_url'] = $vulnerability->value->custom_fields->impacted_url_field;
            $details['impacted_file'] = $vulnerability->value->custom_fields->impacted_file;
            $details['cvss_score'] = $vulnerability->value->custom_fields->cvss_score;
            $details['owasp_field'] = $vulnerability->value->custom_fields->owasp_field;
            $details['payload'] = $vulnerability->value->custom_fields->payload;
            $details['segment'] = (isset($vulnerability->value->custom_fields->segment)) ?$vulnerability->value->custom_fields->segment: null;
            $details['custom_hostname'] = $vulnerability->value->custom_fields->hostname;
            $details['steps_to_reproduce'] = (isset($vulnerability->value->custom_fields->steps_to_reproduce)) ? $this->setCustomListValues($vulnerability->value->custom_fields->steps_to_reproduce): null ;
            $details['request'] = $vulnerability->value->request;
            $details['port'] = ($vulnerability->value->service->ports ?? "");
            $details['tile_bg'] = $this->setSeverityColor($vulnerability->value->severity, $colors, false);
            $details['tile_txt'] = $this->setSeverityColor($vulnerability->value->severity, $colors, true);
            $details['status_bg'] = $this->setStatusColor($vulnerability->value->status, $status_colors, false);
            $details['status_txt'] = $this->setStatusColor($vulnerability->value->status, $status_colors, true);

            $details['response'] = $vulnerability->value->response;

            $finalResult[$host]['results'][$severity][] = $details;
            $this->setVulnerabilityStatus($host, $severity, $vulnerability->value->status);

        }
//        dd($finalResult);

        foreach ($hosts_details->rows as $host){
            $services = json_decode($this->coreController->getServices($this->workspace, $host->id));

            $ip = $host->value->name;
            $finalResult[$ip]['services'] = $services;
            $hostname = "";
            foreach($host->value->hostnames as $h_name) {
                $hostname .= $h_name;
            }
            $finalResult[$ip]['hostname'] = $hostname;
            $finalResult[$ip]['severity'] = $host->value->vulns;

        }
//        dd($finalResult);
        return $this->sortResults($finalResult);
    }

    private function setCustomListValues($fields){
        $data = "<ul>";
        foreach ($fields as $field) {
            $data .= "<li>";
            $data .= $field;
            $data .= "</li>";
        }
        $data .= "</ul>";
        return $data;
    }
    private function setStatusColor($status, $status_colors, bool $text = false) {
        if($status == "open")
            return (!$text) ? $status_colors['open_bg'] : $status_colors['open_txt'];
        else if ($status == "re-opened")
            return (!$text) ? $status_colors['reopen_bg'] : $status_colors['reopen_txt'];
        else if ($status == "closed")
            return (!$text) ? $status_colors['closed_bg'] : $status_colors['closed_txt'];
        else if ($status == "risk-accepted")
            return (!$text) ? $status_colors['riskack_bg'] : $status_colors['riskack_txt'];
    }
    private function setSeverityColor($severity, $colors, bool $text = false)
    {

        if($severity == "med")
            if(!$text) {
               return $colors['medium_bg'];
            } else {
                return $colors['medium_txt'];
            }
        if($severity == "critical") {
            if(!$text) {
                return $colors['critical_bg'];
            } else {
                return $colors['critical_txt'];
            }
        }
        if($severity == "high") {
            if(!$text) {
                return $colors['high_bg'];
            } else {
                return $colors['high_txt'];
            }
        }
        if($severity == "low") {
            if(!$text) {
                return $colors['low_bg'];
            } else {
                return $colors['low_txt'];
            }
        }
        if($severity == "info") {
            if(!$text) {
                return $colors['info_bg'];
            } else {
                return $colors['info_txt'];
            }
        }
        if($severity == "unclassified")
            if(!$text) {
                return $colors['test_bg'];
            } else {
                return $colors['test_txt'];
            }
    }

    private function sortResults(array $finalResult): array
    {
//        dd($finalResult);
        $sortedFinalResult = array();
        $order = array('Critical', 'High', 'Medium', 'Low', 'Informational',  'Test case');
        foreach($finalResult as $key => $value) {
            $sortedFinalResult[$key] = $value;
            if(!isset($value['results']))
                continue;
            $sortedResults = [];
            foreach($order as $severity) {
                if(!isset($value['results'][$severity])) continue;
                $sortedResults[$severity] = $value['results'][$severity];
            }
            $sortedFinalResult[$key]['results'] = $sortedResults;

//            foreach ($results as $severity => $data) {
//                $sortedResults[]
//            }
        }
//        dd($sortedFinalResult);
        return $sortedFinalResult;
    }

}
