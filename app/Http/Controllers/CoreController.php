<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Session;

class CoreController extends Controller
{
    private $client = null;
    private $baseUrl = "report.catalyic.com";
    private $protocol = "http://";

    public function __construct(){
        $this->client= new GuzzleHttp\Client();
    }
    public function checkLogin($token){
        $uri = "/_api/session";
        $cookieJar = CookieJar::fromArray([
            'faraday_session_2' => $token,
        ], $this->baseUrl);
        $url = $this->getUrl($uri);
        try {
            // Your code here.
            $response = $this->client->get($url, ['headers' => $this->getHeaders(), 'cookies' => $cookieJar])->getStatusCode();
            if($response == 200) return true;
        } catch (GuzzleHttp\Exception\RequestException $e) {
            return  false;
        }
    }

    public function doLogin(Request $request) {
        $username = $request->username;
        $password = $request->password;
        $uri = "";
    }

    private function getAuthInformation(){
        $cookieJar = CookieJar::fromArray([
            'faraday_session_2' => Session::get('token'),
        ], $this->baseUrl);
        return $cookieJar;
    }
    private function getHeaders() {
        $headers = array(
            'Host' => $this->baseUrl,
            'Accept' => 'application/json, text/plain, */*',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.67',
            'Referer' => 'http://'.$this->baseUrl,
            'Accept-Encoding' => 'gzip, deflate',
//            'Cookie' => $cookieJar
        );
        return $headers;
    }
    private function getData($uri){
        $url = $this->getUrl($uri);
        try {
            return $this->client->get($url, ['headers' => $this->getHeaders(), 'cookies' => $this->getAuthInformation()])->getBody();
        } catch (GuzzleHttp\Exception\RequestException $e) {

            return  false;
        }
    }
    private function getUrl($api){
       return $this->protocol.$this->baseUrl.$api;
    }
    public function getAllWorkSpaces(){
        $uri = "/_api/v3/ws";
        return $this->getData($uri);
    }
    public function getServices($workspaceName, $hostid){
//        $uri = "/_api/v3/ws/{$workspaceName}/hosts/{$hostid}/services";
        $uri = "/_api/v3/ws/{$workspaceName}/hosts/{$hostid}/services";
//        $uri = "/_api/v3/ws/sample-report/hosts/5/services";
        return $this->getData($uri);
    }
    public function getAllHosts($workspaceName) {
        $uri = "/_api/v3/ws/{$workspaceName}/hosts";
        return $this->getData($uri);
    }
    public function getHost($workspaceName, $obj_id) {
        $uri = "/_api/v3/ws/{$workspaceName}/hosts/{$obj_id}";
        return $this->getData($uri);
    }
    public function getAllVulnerabilities($workspaceName){
        $uri = "/_api/v3/ws/{$workspaceName}/vulns";
        return $this->getData($uri);
    }
    public function getVulnerability($workspaceName, $vulnId){
        $uri = "/_api/v3/ws/{$workspaceName}/vulns/{$vulnId}";
        return $this->getData($uri);
    }
    public function getAttachment($workspaceName, $vulnId){
        $uri = "/_api/v3/ws/{$workspaceName}/vulns/{$vulnId}/attachment";
        return $this->getData($uri);
    }
    public function sampleRequest(){
        $objs = json_decode($this->getAllHosts("pmd_re-assessment_2021"));

        echo print_r($objs);
    }
}
