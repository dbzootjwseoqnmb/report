<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CoreController;
use App\Http\Controllers;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::get("/logout", function(){
    Session::put("token", "");
    return redirect()->route('login');
})->name('logout');
Route::get('/dashboard', [Controllers\DisplayController::class, 'dashboard'])->name('dashboard');
Route::post("/login", [Controllers\DisplayController::class, 'doLogin'])->name('do-login');
Route::get('/test', [CoreController::class, 'sampleRequest'])->name('test');
Route::get('/display', [Controllers\DisplayController::class, 'display'])->name('display');
Route::post('/gen-report', [Controllers\DisplayController::class, 'genreport'])->name('generate-report');
