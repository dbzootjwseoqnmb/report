<html>
<head>
    <title>Excel Sheet</title>
</head>
<body>
<table cellspacing="0" cellpadding="2" border=".1">
    <thead>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">IP</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Hostname</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Port</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Severity</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Vulnerability</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Fixation</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Patch Status</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Target Date</font></th>
        <th bgcolor="#483d8b"><font  face="{{$font_family}}" size="+0.5" color="white">Assigned To</font></th>
    </thead>
    @foreach ($final as $index => $arr)
        @foreach ($arr['report'] as $host => $severities )
            @php
                $hostname = $severities['hostname'];
                if(!isset($severities['results']))
                    continue;
                $sv = $severities['results'];
                foreach($sv as $severity => $vulnerabilities){
                    foreach($vulnerabilities as $vuln){
                @endphp
                @if($severity == "Test case") @php continue; @endphp @endif
                <tr>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p>{{ $host }}</p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p>{{ $hostname }}</p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p>{{ (strlen($vuln['port']) > 0) ? $vuln['port'] : "N/A" }}</p></font>
                    </td>
                    <td class="padding" bgcolor="{{$vuln['tile_bg']}}" style="">
                        <font color="{{$vuln['tile_txt']}}" face="{{$font_family}}" ><p>{{ $severity }}</p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p>{{$vuln['name']}}</p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p>{{ $vuln['resolution'] }}</p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p>{{$vuln['status']}}</p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p></p></font>
                    </td>
                    <td class="padding" style="">
                        <font color="black" face="{{$font_family}}" ><p></p></font>
                    </td>
                </tr>

            @php
                  }
                }
            @endphp

        @endforeach
    @endforeach
</table>
</body>
</html>
