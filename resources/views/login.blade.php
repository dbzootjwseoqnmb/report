<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>My Login Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body class="my-login-page">
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                <div class="brand">
                    <br>
                    <br>
                    <br>
                    <img src="https://i1.wp.com/catalyicsecurity.com/wp-content/uploads/2021/04/catalyic-security.png?fit=992%2C273&ssl=1" width="250" alt="logo">
                    <br>
                    <br>
                    <br>
                </div>
                <div class="card fat">
                    <div class="card-body">
                        <h4 class="card-title">Login</h4>
                        <form method="POST" class="my-login-validation" action="{{route('do-login')}}" novalidate="">
                            <div class="form-group">
                                @if(isset($error))
                                    <div class="alert alert-danger">{{$error}}</div>
                                @endif
                                <label for="email">Token</label>
                                <input id="email" type="text" class="form-control" name="token" value="" required autofocus>
                                <div class="invalid-feedback">
                                    Token is invalid
                                </div>
                            </div>

                            {{csrf_field()}}

                            <div class="form-group">
                                <div class="custom-checkbox custom-control">
                                    <input type="checkbox" name="remember" id="remember" class="custom-control-input">
                                    <label for="remember" class="custom-control-label">Remember Me</label>
                                </div>
                            </div>

                            <div class="form-group m-0">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Login
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="footer">
                    Copyright &copy; 2021 &mdash;
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>

