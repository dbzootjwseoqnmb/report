<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Report</title>
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="assets/css/sheets-of-paper-a4.css">
        <style>
            span.pre {
                max-width: 18%;
                display: inline-block;
                white-space: pre-wrap;
                font-family: monospace;
                margin: 1em 0;
                text-align: justify;
                word-wrap: break-word;
                position: fixed;
            }
            .poc-img {
                max-width: 490px;
            }
            .padding {
                padding: 1rem;
            }
            .poc-img > img {
                width: 100%;
                height: 100%;
            }
            /*.table {*/
            /*    border: #1a202c 1px solid;;*/
            /*    width: 100%;*/
            /*    max-width: 100%;*/
            /*    padding: 10px 10px;*/
            /*}*/
            /*.table tr {*/
            /*    border: #1a202c 1px solid;;*/
            /*}*/
            p { word-break: break-all }
            pre {
                overflow-x: hidden;
                max-width: 17%;
                white-space: pre-wrap;
                white-space: -moz-pre-wrap;
                white-space: -o-pre-wrap;
                word-wrap: break-word;
            }
            body {
                margin-top: 1.5cm !important;
                margin-bottom: 1.5cm !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                /*padding: 0 !important;*/
            }
            @media screen {
                div.divFooter {
                    display: block;
                }
                div.divHeader {
                     display: block;
                 }
            }
            @media print {
                div.divFooter {
                    position: fixed;
                    bottom: 0;
                    margin-top: 1.5cm;
                }
                div.divHeader {
                    position: fixed;
                    top: 0;
                    margin-bottom: 1.5cm;
                }
            }

            table.CSSTable td, table.CSSTable th {
                border:  0.5px solid #999;
                /*padding: 0.5rem;*/
                text-align: left;
                margin: 0 !important;
            }
            tr {
                margin: 0 !important;
                padding: 0 !important;
            }
            table.CSSTable {
                display: table;
                border-collapse: separate;
                border-spacing: 2px;
                border-color: gray;
            }
            tbody {
                padding: 0 !important;
                margin: 0 !important;
                max-width: 100%;
                min-width: 100%;
                display:block;
            }
        </style>
    </head>
    <body class="document">
        <div class="page" contenteditable="true">

            @foreach($final as $index => $report)
                @php
                    $title = $report['title'];
                    $vulns = $report['report'];
                @endphp
                <font color="black" face="{{$font_family}}" size="+0.5"><h1>{{$title}}</h1></font>
                @foreach($vulns as $host => $severities)

                    <h3>
                        <font color="black" face="{{$font_family}}" size="+2">{{$host}} @if($severities['hostname'] != "") ({{$severities['hostname']}}) @endif </font>
                    </h3>

                    @if(isset($severities['results']))
                        <table cellpadding="5" cellspacing="0" style="border-collapse: collapse; width:100%;">
                            <tr>
                                <td style="background-color: #424242; color: white; font-family: {{$font_family}}; font-weight: bold; text-align: center;" rowspan="2" colspan="1">Status</td>
                                <td style="background-color: #424242; color: white; font-family: {{$font_family}}; font-weight: bold; text-align: center;" colspan="6">Vulnerabilities Count</td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid; color: {{$colors['critical_txt']}}; font-family: {{$font_family}}; font-weight: bold; text-align: center; background-color: {{$colors['critical_bg']}};">Critical</td>
                                <td style="border: 1px solid; color:  {{$colors['high_txt']}}; font-family: {{$font_family}}; font-weight: bold; text-align: center; background-color: {{$colors['high_bg']}};">High</td>
                                <td style="border: 1px solid; color: {{$colors['medium_txt']}}; font-family: {{$font_family}}; font-weight: bold; text-align: center; background-color: {{$colors['medium_bg']}};">Medium</td>
                                <td style="border: 1px solid; color: {{$colors['low_txt']}}; font-family: {{$font_family}}; font-weight: bold; text-align: center; background-color: {{$colors['low_bg']}};">Low</td>
                                <td style="border: 1px solid; color: {{$colors['info_txt']}}; font-family: {{$font_family}}; font-weight: bold; text-align: center; background-color: {{$colors['info_bg']}};">Info.</td>
                                <td style="border: 1px solid; color: {{$colors['test_txt']}}; font-family: {{$font_family}}; font-weight: bold; text-align: center; background-color: {{$colors['test_bg']}};">Test Cases</td>

                            </tr>
                            @php
                                $tempStatus = array('open', 're-opened', 'closed', 'risk-accepted');
                            @endphp
                            @foreach($tempStatus as $state)
                                <tr>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; font-weight: bold;" >{{ ucwords($state)}}</td>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center;" >{{ ($vStatus[$host]['Critical'][$state] ?? 0)  }}</td>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center;" >{{ ($vStatus[$host]['High'][$state] ?? 0)  }}</td>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center;" >{{ ($vStatus[$host]['Medium'][$state] ?? 0)  }}</td>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center;" >{{ ($vStatus[$host]['Low'][$state] ?? 0)  }}</td>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center;" >{{ ($vStatus[$host]['Informational'][$state] ?? 0)  }}</td>
                                    <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center;" >{{ ($vStatus[$host]['Test case'][$state] ?? 0)  }}</td>

                                </tr>

                            @endforeach

                            <tr>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; font-weight: bold;" >Total</td>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center; font-weight: bold;" >{{ (isset($severities['results']['Critical']) ? sizeof($severities['results']['Critical']) : 0)  }}</td>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center; font-weight: bold;" >{{ (isset($severities['results']['High']) ? sizeof($severities['results']['High']) : 0)  }}</td>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center; font-weight: bold;" >{{ (isset($severities['results']['Medium']) ? sizeof($severities['results']['Medium']) : 0)  }}</td>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center; font-weight: bold;" >{{ (isset($severities['results']['Low']) ? sizeof($severities['results']['Low']) : 0)  }}</td>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center; font-weight: bold;" >{{ (isset($severities['results']['Informational']) ? sizeof($severities['results']['Informational']) : 0)  }}</td>
                                <td style="border: 1px solid; color: black; font-family: {{$font_family}}; text-align: center; font-weight: bold;" >{{ (isset($severities['results']['Test case']) ? sizeof($severities['results']['Test case']) : 0)  }}</td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                    @endif

                    @if(sizeof((array) $severities['services']) > 0)
                    <div>
                        <span style="color: black; font-family: {{$font_family}}; font-weight: bold;">Identified Ports</span>
                        <br>
                        <br>
                        <table cellpadding="15" cellspacing="0" style="width:100%; border-collapse: collapse;">
                            <tr>
                                <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}}; font-weight: bold;">#</span></td>
                                <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}}; font-weight: bold;">Port</span></td>
                                <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}}; font-weight: bold;">Name</span></td>
                                <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}}; font-weight: bold;">Version</span></td>
                                <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}}; font-weight: bold;">Status</span></td>
                            </tr>
                            @php
                                $i = 1;
                                $services = $severities['services'];

                            @endphp

                            @php
                                foreach( (array)$services as $port) {
                            @endphp

                                <tr >
                                    <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}};">{{$i}}</span></td>
                                    <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}};">{{$port->port}}</span></td>
                                    <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}};">{{$port->name}}</span></td>
                                    <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}};">{{$port->version}}</span></td>
                                    <td style="border: 1px solid;" colspan="2"><span style="color: black; font-family: {{$font_family}};">{{$port->status}}</span></td>

                                </tr>
                            @php
                                $i +=1;
                                }
                            @endphp
                        </table>
                    </div>
                    @endif
                    @if (!isset($severities['results']))
                        <div>
                            <br>
                            <font color="black" face="{{$font_family}}" size="+1.5">
                                No vulnerabilities found
                            </font>
                        </div>
                        @php
                            continue;
                        @endphp
                    @endif

                    @foreach($severities['results'] as $severity => $vulnerabilities)

                        @foreach($vulnerabilities as $vuln)
                            <div>
                                <h4>
                                    <font color="black" face="{{$font_family}}" size="+1.5"><span style="font-weight: bold;">{{$vuln['name']}}</span></font>
                                </h4>
                                <table class="CSSTable" cellpadding="15" cellspacing="0" style="border:  0.5px solid black; border-collapse: collapse;">
                                    <tr style="background-color: {{$vuln['tile_bg']}};"class="">
                                        <td class="padding" style="border:  0.5px solid black;">
                                            <font color="{{$vuln['tile_txt']}}" face="{{$font_family}}" size="+0.5"><span style="font-weight: bold;">{{$severity}}</span></font>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding" style="border:  0.5px solid black;">
                                            <font color="black" face="{{$font_family}}" size="+0.5"><span style="font-weight: bold;">Description</span></font> <br>
                                            <div>
                                                <font color="black" face="{{$font_family}}" ><p>{!! nl2br($vuln['description']) !!}</p></font>
                                            </div>
                                        </td>
                                    </tr>

                                    @if(strlen($vuln['custom_hostname']) > 0)
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Hostname
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{!! nl2br($vuln['custom_hostname']) !!}</font></div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if(strlen($vuln['port']) > 0)
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Service Port
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{{$vuln['port']}}</font></div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if(strlen($vuln['cvss_score']) > 0)
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            CVSS Score
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{{$vuln['cvss_score']}}</font></div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if(strlen($vuln['owasp_field']) > 0)
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            OWASP Top 10
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{{$vuln['owasp_field']}}</font></div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if(count((array) $vuln['impacted_file']))
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Impacted File
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{{$vuln['impacted_file']}}</font></div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if(count((array) $vuln['payload']))
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Payload
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{{$vuln['payload']}}</font></div>
                                            </td>
                                        </tr>
                                    @endif
                                    @if(count((array) $vuln['segment']))
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Segment
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{{$vuln['segment']}}</font></div>
                                            </td>
                                        </tr>
                                    @endif

                                    @if(count((array) $vuln['impacted_url']))
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Impacted URL
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{!! nl2br($vuln['impacted_url']) !!}</font></div>
                                            </td>
                                        </tr>
                                    @endif

                                    @if(count((array) $vuln['steps_to_reproduce']))
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}" size="+0.5">
                                                        <strong>
                                                            Steps to Reproduce
                                                        </strong>
                                                    </font>
                                                </div>
                                                <div><font color="black" face="{{$font_family}}" >{!! nl2br($vuln['steps_to_reproduce']) !!}</font></div>
                                            </td>
                                        </tr>
                                    @endif

                                    @if(count((array) $vuln['attachments']) > 0)
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <div><font color="black" face="{{$font_family}}"  size="+0.5"><span style="font-weight: bold;">Proof of Concept</span></font></div>
                                                @foreach($vuln['attachments'] as $name => $attachment)
                                                    <div class="poc-img">
                                                        <img src="data:{{$attachment->content_type}};base64, {{$attachment->data}}"   />
                                                    </div>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endif


                                    @if(strlen($vuln['request']) > 0)
                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <p><font color="black" face="{{$font_family}}" size="+0.5" ><span style="font-weight: bold;">Request</span></font></p>
                                                <span>
                                                        <pre>{{$vuln['request']}}</pre>
                                                    </span>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="padding" style="border:  0.5px solid black;">

                                                <p><font color="black" face="{{$font_family}}" size="+0.5" ><span style="font-weight: bold;">Response</span></font></p>
                                                <div>
                                                    <pre>{{$vuln['response']}}</pre>
                                                </div>

                                            </td>
                                        </tr>
                                    @endif
                                    @if(strlen($vuln['resolution']) > 0)

                                    <tr>
                                        <td class="padding" style="border:  0.5px solid black;">

                                            <font color="black" face="{{$font_family}}" size="+0.5"><span style="font-weight: bold;">Remediation</span></font>
                                            <div><font color="black" face="{{$font_family}}" >{!! nl2br($vuln['resolution']) !!}</font></div>
                                        </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td class="padding" style="background: {{$vuln['status_bg']}};border:  0.5px solid black;">

                                            <div><font color="{{$vuln['status_txt']}}" face="{{$font_family}}" size="+0.5" ><span style="font-weight: bold;">Status</span></font></div>
                                            <div><font color="{{$vuln['status_txt']}}" face="{{$font_family}}" >{{$vuln['status']}}</font></div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                            <br>
                            <br>
                            <br>
                        @endforeach
                        <div class="page-break" contenteditable="false"></div>
                    @endforeach

                @endforeach
                <div class="page-break" contenteditable="false"></div>

            @endforeach
        </div>

    </body>
</html>
