<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Reporting Tool</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/iconfonts/ionicons/dist/css/ionicons.css">
    <link rel="stylesheet" href="assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/demo_1/style.css">
    <!-- End Layout styles -->

    <style>
        body, table, tr, td{
            word-wrap: break-word;
        }
    </style>
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a class="navbar-brand brand-logo" href="#">
                <img src="https://i0.wp.com/catalyicsecurity.com/wp-content/uploads/2021/03/catalyic-logo-white.png?fit=161%2C45&ssl=1" alt="logo" /> </a>
            <a class="navbar-brand brand-logo-mini" href="#">
                <img src="https://i0.wp.com/catalyicsecurity.com/wp-content/uploads/2021/03/catalyic-logo-white.png?fit=161%2C45&ssl=1" alt="logo" /> </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            <ul class="navbar-nav">
                <li class="nav-item font-weight-semibold d-none d-lg-block">Help : +92 332 6347757</li>

            </ul>


            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item nav-profile">
                    <a href="#" class="nav-link">
                        <div class="profile-image">
                            <img class="img-xs rounded-circle" src="assets/images/faces/face7.jpg" alt="profile image">
                            <div class="dot-indicator bg-success"></div>
                        </div>
                        <div class="text-wrapper">
                            <p class="profile-name">Admin</p>
                            <p class="designation">Admin User</p>
                        </div>
                    </a>
                </li>
                <li class="nav-item nav-category">Main Menu</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('dashboard')}}">
                        <i class="menu-icon typcn typcn-document-text"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('logout')}}">
                        <i class="menu-icon typcn typcn-document-text"></i>
                        <span class="menu-title">Logout</span>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- partial -->

        <div class="main-panel">
            <form action="{{route('generate-report')}}" target="_blank" method="post">

                <div class="content-wrapper">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h3>Report Configurations</h3>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Critical Clr</th>
                                            <th>Critical Txt Clr</th>

                                            <th>High Clr</th>
                                            <th>High Txt Clr</th>

                                            <th>Medium Clr</th>
                                            <th>Medium Txt Clr</th>

                                            <th>Low Clr</th>
                                            <th>Low Txt Clr</th>

                                            <th>Info Clr</th>
                                            <th>Info Txt Clr</th>

                                            <th>Test Clr</th>
                                            <th>Test Txt Clr</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" name="critical_bg" value="#C00000">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="critical_txt" value="white">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name="high_bg" value="#E73104">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="high_txt" value="white">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name="medium_bg" value="#FFC000">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="medium_txt" value="white">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name="low_bg" value="#0070C0">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="low_txt" value="white">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name="info_bg" value="#61B5FF">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="info_txt" value="white">
                                            </td>

                                            <td>
                                                <input type="text" class="form-control" name="test_bg" value="#92D050">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="test_txt" value="white">
                                            </td>
                                        </tr>

                                        <tr>
                                            <th colspan="4">Font Family</th>

                                            <th>Open Clr</th>
                                            <th>Open Txt Clr</th>

                                            <th>Re-Open Clr</th>
                                            <th>Re-Open Txt Clr</th>

                                            <th>Closed Clr</th>
                                            <th>Closed Txt Clr</th>

                                            <th>Risk-Accepted Clr</th>
                                            <th>Risk-Accepted Txt Clr</th>
                                        </tr>

                                        <tr>
                                            <td colspan="4"><input type="text" class="form-control" name="font_family" value="Gadugi, Verdana, Geneva, sans-serif"></td>

                                            <td><input type="text" class="form-control" name="open_bg" value="white"></td>
                                            <td><input type="text" class="form-control" name="open_txt" value="black"></td>


                                            <td><input type="text" class="form-control" name="reopen_bg" value="white"></td>
                                            <td><input type="text" class="form-control" name="reopen_txt" value="black"></td>


                                            <td><input type="text" class="form-control" name="closed_bg" value="#31849B"></td>
                                            <td><input type="text" class="form-control" name="closed_txt" value="white"></td>


                                            <td><input type="text" class="form-control" name="riskack_bg" value="#764CAE"></td>
                                            <td><input type="text" class="form-control" name="riskack_txt" value="white"></td>



                                        </tr>


                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="card">

                                <div class="card-body">
                                    <h3>Generate Report</h3>
                                    <br>
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>#</th>
                                        <th>Workspace</th>
                                        <th>Critical</th>
                                        <th>High</th>
                                        <th>Medium</th>
                                        <th>Low</th>
                                        <th>Info.</th>
                                        <th>Passed</th>
                                        <th>Total</th>
                                        <th>Include</th>
                                        </thead>
                                        <tbody>
                                        @php($i = 1)
                                        @foreach($workspaces as $workspace)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <input type="hidden" name="titles[]" class="form-control" >
                                                <td>{{$workspace->name}} </td>
                                                <td>{{$workspace->stats->critical_vulns}} </td>
                                                <td>{{$workspace->stats->high_vulns}} </td>
                                                <td>{{$workspace->stats->medium_vulns}} </td>
                                                <td>{{$workspace->stats->low_vulns}} </td>
                                                <td>{{$workspace->stats->info_vulns}} </td>
                                                <td>{{$workspace->stats->unclassified_vulns}} </td>
                                                <td><strong>{{($workspace->stats->critical_vulns + $workspace->stats->high_vulns + $workspace->stats->medium_vulns + $workspace->stats->low_vulns )}} </strong></td>
                                                <td> <span><input type="checkbox" name="checkboxes[{{$workspace->name}}]" value="{{$workspace->name}}" class="form-check-input" ></span> </td>
                                                <input type="hidden" value="{{$workspace->name}}" name="workspaces[]" class="form-control" >

                                            </tr>
                                            @php($i +=1)

                                        @endforeach
                                        </tbody>
                                    </table>

                                    <input class="btn btn-success"  type="submit" name="word" value="Generate Word">
                                    <input class="btn btn-primary"  type="submit" name="excel" value="Generate Excel">
                                    {{csrf_field()}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">

            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>

</body>
</html>

